import React from "react";
import styles from './SliderBtn.module.scss'
import cn from 'classnames'
import { ArrowSvg } from "../../icons/ArrowSvg";

const SliderBtnComponent = ({direction, onClick }) => (
    <button className={cn(styles.sliderBtn, styles[direction])} onClick={onClick}>
        <ArrowSvg />
    </button>
)

export const SliderBtn = React.memo(SliderBtnComponent)