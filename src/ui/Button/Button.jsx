import React from "react";
import styles from './Button.module.scss'
import cn from 'classnames'

const ButtonComponent = ({onClick, children, style}) => (
    <button className={cn(style, styles.button)} onClick={onClick}>
        {children}
    </button>
)

export const Button = React.memo(ButtonComponent)