import React from "react";
import { Button } from "../Button/Button";
import styles from './ItemCard.module.scss'

const ItemCardComponent = ({ card: {pic, title, desc, date, entity}, onClick }) => (
    <div className={styles.card}>
        <div className={styles.card__top}>
            <div className={styles.card__pic}>
                <img src={pic} alt={title} />
            </div>
            <span className={styles.card__entity}>{entity}</span>
        </div>
        <div className={styles.card__info}>
            <h2 className={styles.card__title}>{title}</h2>
            <span className={styles.card__desc}>{desc}</span>
        </div>
        <div className={styles.card__action}>
            <span className={styles.card__date}>{date}</span>
            <Button onClick={onClick}>Смотреть</Button>
        </div>
    </div>
)

export const ItemCard = React.memo(ItemCardComponent)