import React from "react";
import { Link } from "react-router-dom"
import styles from './LinkUi.module.scss'
import cn from 'classnames'

const LinkUiComponent = ({style, href, children}) => (
    <Link to={href} className={cn(style, styles.link)}>{children}</Link>                           
)

export const LinkUi = React.memo(LinkUiComponent)