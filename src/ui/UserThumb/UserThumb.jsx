import React from 'react'
import styles from './UserThumb.module.scss'
import cn from 'classnames'
import { TextUi } from '../Text/Text'

const UserThumbComponent = ({user: {avatar, name, status, pro} }) => (
    <div className={cn(styles.userThumb, pro && styles.userThumb_pro)}>
        <div className={styles.userThumb__avatar}>
            <img src={avatar}/>
        </div>
        <div className={styles.userInfo} >
            <TextUi style={styles.userInfo__name}>{name}</TextUi>
            <span className={styles.userInfo__status}>{status}</span>
        </div>
        {pro && <span className={styles.userInfo__pro}>PRO</span>}
    </div>
)

export const UserThumb = React.memo(UserThumbComponent)