import React from "react";
import styles from "./Factoid.module.scss";
import cn from "classnames"
import { EnergySvg } from "./EnergySvg";

const FactoidComponent = ({ style, withIcon, children }) => (
    <div className={cn(style, styles.factoid, withIcon && styles.iconFactoid)}>
        {withIcon && (
            <div className={styles.factoid__icon}>
                <EnergySvg />
            </div>
        )}
        <span>{children}</span>
    </div>
)

export const Factoid = React.memo(FactoidComponent)