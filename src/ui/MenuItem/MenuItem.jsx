import React, { useState } from "react";
import { Link } from "react-router-dom"
import { ArrowSvg } from "../../icons/ArrowSvg";
import styles from  './MenuItem.module.scss'
import cn from 'classnames'

const MenuItemComponent = ({item: {title, icon, link, items, active}, children }) => {
    const [ itemActive, setItemActive ] = useState(active)

    const handleItemClick = () => {
        setItemActive((oldState) => !oldState)
    }
    return (
        <>
            <li className={cn(styles.menuItem, icon && styles.menuItem__withIcon, itemActive && styles.menuItem__active)}>
                {items.length ? (
                    <span className={cn(styles.menuItem__link, itemActive && styles.opened )} onClick={handleItemClick}>
                        {title}
                        <ArrowSvg style={styles.menuItem__arrow}/>
                    </span>
                ): (
                    <Link to={link} className={styles.menuItem__link} >
                        {icon}
                        {title}
                    </Link>
                )}
            </li>
            {itemActive && children}
        </>
    )
}

export const MenuItem = React.memo(MenuItemComponent)