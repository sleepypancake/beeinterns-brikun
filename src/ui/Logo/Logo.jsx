import React from "react";
import styles from './Logo.module.scss'
import { LogoSvg } from "../../icons/LogoSvg";

const LogoComponent = () => (
    <div className={styles.logo}>
        <LogoSvg />
    </div>
)

export const Logo = React.memo(LogoComponent)