import React from "react";
import styles from './Text.module.scss'
import cn from 'classnames'

const TextComponent = ({style, children}) => (
    <p className={cn(style, styles.text)}>{children}</p>
)

export const TextUi = React.memo(TextComponent)