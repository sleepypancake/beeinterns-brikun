import React from "react";
import { Factoid } from "../../ui/Factoid/Factoid";
import { LinkUi } from "../../ui/LinkUi/LinkUi";
import { UserThumb } from "../../ui/UserThumb/UserThumb";
import styles from "./TopMenu.module.scss"
import { USER } from "../../utils/FakeApi/data";

const TopMenuComponent = () => (
    <div className={styles.topmenu__container}>
        <div className={styles.topmenu}>
            <LinkUi href='/' style={styles.topmenu__link}>FAQ</LinkUi>
            <LinkUi href='/' style={styles.topmenu__link}>Поддержка</LinkUi>
            <LinkUi href='/' style={styles.topmenu__link}>Настройки</LinkUi>
        </div>
        <div className={styles.mainInfo}>
            <div className={styles.mainInfo__factoids}>
                <Factoid style={styles.mainInfo__factoid}>15 готовых дз</Factoid>
                <Factoid style={styles.mainInfo__factoid}>18 лекций</Factoid>
                <Factoid style={styles.mainInfo__factoid} withIcon>
                    <span className={styles.mainInfo__factoidNum}>1000</span> часов</Factoid>
            </div>
            <div className={styles.user}>
                <UserThumb user={USER} />
            </div>
        </div>
    </div>
)

export const TopMenu = React.memo(TopMenuComponent)