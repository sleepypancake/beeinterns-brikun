import React from "react";
import cn from 'classnames';
import { Logo } from "../../ui/Logo/Logo";
import styles from './SideBar.module.scss'
import { MENU_LINKS } from '../../utils/FakeApi/data'
import { ArrowSvg } from "../../icons/ArrowSvg";
import { MenuItem } from '../../ui/MenuItem/MenuItem'

const SideBarComponent = () => (
    <div className={styles.sidebar}>
        <Logo />
        <ul  className={cn(styles.sidebar__menu, styles.sidebar__menu_first)}>
            {MENU_LINKS.map(item => (
                <MenuItem key={item.id} item={item}>
                    {item.items.length ? (
                        <ul className={cn(styles.sidebar__menu, styles.sidebar__menu_second)}>
                            {item.items.map(item => (
                                <MenuItem key={item.id} item={item}>
                                    {item.items.length ? (
                                        <ul className={cn(styles.sidebar__menu, styles.sidebar__menu_third)}>
                                            {item.items.map(item => (
                                                <MenuItem key={item.id} item={item} />
                                            ))}
                                        </ul>
                                    ) : <></>}
                                </MenuItem>
                            ))}
                        </ul>
                    ) : <></>}
                </MenuItem>
            ))}
        </ul>
        
    </div>
)

export const SideBar = React.memo(SideBarComponent)