import React from "react";
import { Button } from "../../ui/Button/Button";
import { ItemCard } from "../../ui/ItemCard/ItemCard";
import { SliderBtn } from "../../ui/SliderBtn/SliderBtn";
import styles from './ItemCards.module.scss'

const ItemCardsComponents = ({ cards, title, subtitle }) => (
    <div className={styles.itemCards}>
        <div className={styles.itemCards__infoWrapper}>
            <div className={styles.itemCards__info}>
                <span className={styles.itemCards__subtitle}>{subtitle}</span>
                <span className={styles.itemCards__title}>{title}</span>
            </div>
            <div className={styles.itemCards__listing}>
                <Button style={styles.itemCards__btn}>Все лекции</Button>
                <SliderBtn direction="left"/> 
                <SliderBtn direction="right"/> 
            </div>
        </div>
        <div className={styles.list}>
            {cards.map(card => (
                <ItemCard
                    key ={card.id}
                    card = {card}
                />
            ))}
        </div>
    </div> 
)

export const ItemCards = React.memo(ItemCardsComponents)