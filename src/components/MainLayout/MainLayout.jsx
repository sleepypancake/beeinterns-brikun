import React from "react";
import { ItemCards } from "../ItemCards/ItemCards";
import styles from './MainLayout.module.scss'
import { CARDS } from "../../utils/FakeApi/data";
import { SideBar } from "../SideBar/SideBar";
import { TopMenu } from "../TopMenu/TopMenu";

const MainLayoutComponent = () => (
    <div className={styles.container}>
        <div className={styles.sidebar}>
            <SideBar />
        </div>
        <div className={styles.content}>
            <TopMenu />
            <div className={styles.content__wrapper}>
                <ItemCards cards={CARDS.html} subtitle="Базовый уровень" title="HTML/CSS"/>
                <ItemCards cards={CARDS.html} subtitle="продвинутый уровень" title="HTML/CSS" />
                <ItemCards cards={CARDS.html} subtitle="продвинутый уровень" title="HTML/CSS" />
                <ItemCards cards={CARDS.html} subtitle="продвинутый уровень" title="HTML/CSS" />
            </div>
        </div>
    </div>
)

export const MainLayout = React.memo(MainLayoutComponent)