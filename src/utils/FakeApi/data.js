import { AngularSvg } from "../../icons/AngularSvg"
import { DataSvg } from "../../icons/DataSvg"
import { GitSvg } from "../../icons/GitSvg"
import { HtmlSvg } from "../../icons/HtmlSvg"
import { JsSvg } from "../../icons/JsSvg"
import { ReactSvg } from "../../icons/ReactSvg"

export const CARDS = {
    html: [
        {
            id: 0,
            pic: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/202012/chris-ried-ieic5Tq8YMk-unsplas_1200x768.jpeg?bEhcYQAShJnLf0Mtu4JYq8YzICfhz2rB&size=770:433', 
            title: 'Структура HTML', 
            desc: 'Назначение блоков: head, body',
            date: '01.02.2022',
            entity: 'Лекция',
        },
        {
            id: 1,
            pic: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/202012/chris-ried-ieic5Tq8YMk-unsplas_1200x768.jpeg?bEhcYQAShJnLf0Mtu4JYq8YzICfhz2rB&size=770:433', 
            title: 'Подключение стилей', 
            desc: 'Назначение блоков: head, body Назначение блоков: head, body',
            date: '01.02.2022',
            entity: 'Лекция',
        },
        {
            id: 2,
            pic: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/202012/chris-ried-ieic5Tq8YMk-unsplas_1200x768.jpeg?bEhcYQAShJnLf0Mtu4JYq8YzICfhz2rB&size=770:433', 
            title: 'Наследие и каскад', 
            desc: 'Назначение блоков: head, body',
            date: '01.02.2022',
            entity: 'Лекция',
        },
        {
            id: 3,
            pic: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/202012/chris-ried-ieic5Tq8YMk-unsplas_1200x768.jpeg?bEhcYQAShJnLf0Mtu4JYq8YzICfhz2rB&size=770:433', 
            title: 'Наследие и каскад', 
            desc: 'Назначение блоков: head, body',
            date: '01.02.2022',
            entity: 'Лекция',
        },
    ]
}

export const USER = {
    avatar: 'https://images.unsplash.com/photo-1608889175123-8ee362201f81?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
    name: 'Вася Пупкин',
    status: 'Online',
    pro: true
}

export const MENU_LINKS = [
    {
        id: 0,
        title:  'Программа',
        link: '/program',
        active: false,
        items: [
            {
                id: 0,
                title:  'МЕНТОРЫ',
                link: '/mentors',
                active: false,
                items: []
            },
            {
                id: 1,
                title:  'ЛЕКЦИИ',
                link: '/lectures',
                active: false,
                items: [
                    {
                        id: 0,
                        title:  'HTML/CSS',
                        link: '/lectures/html',
                        icon: <HtmlSvg />,
                        active: true,
                        items: []
                    },
                    {
                        id: 1,
                        title:  'GIT',
                        link: '/lectures/git',
                        icon: <GitSvg />,
                        active: false,
                        items: []
                    },
                    {
                        id: 2,
                        title:  'JavaScript',
                        link: '/lectures/js',
                        icon: <JsSvg />,
                        active: false,
                        items: []
                    },
                    {
                        id: 3,
                        title:  'React',
                        link: '/lectures/react',
                        icon: <ReactSvg />,
                        active: false,
                        items: []
                    },
                    {
                        id: 4,
                        title:  'Angular',
                        link: '/lectures/angular',
                        icon: <AngularSvg />,
                        active: false,
                        items: []
                    },
                    {
                        id: 5,
                        title:  'Data',
                        link: '/lectures/data',
                        icon: <DataSvg />,
                        active: false,
                        items: []
                    },
                ]
            },

        ]
    },
    {
        id: 1,
        title:  'ПРАВИЛА ПОСТУПЛЕНИЯ',
        link: '/rules',
        active: false,
        items: []
    },
    {
        id: 2,
        title:  'НОВОСТИ',
        link: '/news',
        active: false,
        items: []
    },
    {
        id: 3,
        title:  'ОБРАТНАЯ СВЯЗЬ',
        link: '/feedback',
        active: false,
        items: []
    },
]